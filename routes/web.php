<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'RedirectController@redirectToAdminPanel');
Route::get('/login', 'RedirectController@getLogin')->name('login');


Route::prefix('mobile')->group(function () {
    Route::get('login_phone', 'Mobile\LoginController@loginPhone');
});