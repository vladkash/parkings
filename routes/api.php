<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function () {
    Route::post('/send_sms', 'API\Auth\SmsController@sendAuthSms');
    Route::post('/check_phone', 'API\Auth\PhoneController@check');
    Route::post('/register', 'API\Auth\AuthController@register');
    Route::post('/login', 'API\Auth\AuthController@login');
});

Route::middleware('auth:api')->group(function () {
    Route::prefix('parkings')->group(function () {
        Route::get('/', 'API\Parking\ParkingController@index');
        Route::get('/{id}', 'API\Parking\ParkingController@show');
        Route::post('/search', 'API\Parking\ParkingController@search');
    });
    Route::get('/current_session','API\Parking\ParkingController@currentSession');
    Route::prefix('sessions')->group(function () {
        Route::get('/', 'API\Parking\ParkingController@sessions');
        Route::post('/start', 'API\Parking\ParkingController@start');
        Route::post('/stop', 'API\Parking\ParkingController@stop')->middleware('check_parking');
    });
    Route::prefix('payments')->group(function () {
        Route::get('/', 'API\Payment\PaymentController@index');
    });
    Route::prefix('banner')->group(function () {
        Route::get('/', 'API\Banner\BannerController@show');
    });

    Route::post('/callback', 'API\Callback\CallbackController@callback');

    Route::resource('cars', 'API\Car\CarController')->only([
        'index', 'store', 'show', 'update', 'destroy'
    ]);

    Route::post('/update_profile', 'API\Client\ClientController@update');

});
