<?php

return [
    'username' => env('GOIP_USERNAME'),
    'password' => env('GOIP_PASSWORD')
];