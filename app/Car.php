<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $guarded = [];

    protected $casts = [
        'isspecial' => 'boolean',
        'ismain' => 'boolean'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function parkings()
    {
        return $this->hasMany(ClientParking::class);
    }
}
