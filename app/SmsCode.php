<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsCode extends Model
{
    protected $guarded = [];

    public static function createCode($phone)
    {
        $code = rand(1001, 9999);

        static::updateOrCreate(
            ['phone'=>$phone],
            ['code'=>$code]
        );

        return $code;
    }

    public static function checkCode($phone, $code)
    {
        return static::where([
            'phone'=>$phone,
            'code'=>$code
        ])->delete();
    }
}
