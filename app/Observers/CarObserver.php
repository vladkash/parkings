<?php

namespace App\Observers;

use App\Car;

class CarObserver
{
    /**
     * Handle the car "created" event.
     *
     * @param  \App\Car  $car
     * @return void
     */
    public function created(Car $car)
    {
        if ($car->ismain) {
            $car->client->cars()->where('id', '!=', $car->id)->where('ismain', true)->update(['ismain'=>false]);
        }
    }

    /**
     * Handle the car "updated" event.
     *
     * @param  \App\Car  $car
     * @return void
     */
    public function updated(Car $car)
    {
        if ($car->ismain) {
            $car->client->cars()->where('id', '!=', $car->id)->where('ismain', true)->update(['ismain'=>false]);
        }
    }

    /**
     * Handle the car "deleted" event.
     *
     * @param  \App\Car  $car
     * @return void
     */
    public function deleted(Car $car)
    {
        if ($car->ismain) {
            $car->client->cars()->where('id', '!=', $car->id)->update(['ismain'=>true]);
        }
    }

    /**
     * Handle the car "restored" event.
     *
     * @param  \App\Car  $car
     * @return void
     */
    public function restored(Car $car)
    {
        //
    }

    /**
     * Handle the car "force deleted" event.
     *
     * @param  \App\Car  $car
     * @return void
     */
    public function forceDeleted(Car $car)
    {
        //
    }
}
