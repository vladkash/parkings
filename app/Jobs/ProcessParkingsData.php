<?php

namespace App\Jobs;

use App\Parking;
use Grimzy\LaravelMysqlSpatial\Types\LineString;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProcessParkingsData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 172800;

    protected $data;

    protected $databaseColumns = [
        'place_id',
        'place_name',
        'address'
    ];
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = json_decode($data, true);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        ini_set('max_execution_time', 172800);
        $this->processData();
    }

    protected function processData()
    {
        foreach ($this->data as $parkingData) {
            $parkingData['place_id'] = $parkingData['Cells']['ParkingZoneNumber'];
            $parkingData['place_name'] = $parkingData['Cells']['ParkingName'];
            $parkingData['address'] = $parkingData['Cells']['Address'];
            $parkingData['place_point'] = $this->makePoint($parkingData['Cells']['geoData']['coordinates']);
            $parkingData['place_coordinates'] = $this->makeCoordinates($parkingData['Cells']['geoData']['coordinates']);
            $parking = Parking::checkForExists($parkingData['Cells']['ParkingName']);
            if ($parking) {
                try {
                    $parking->update(array_only($parkingData, $this->databaseColumns));
                    DB::update('update parkings set place_coordinates = GeomFromText(?),place_point=GeomFromText(?) where id = ?',[$parkingData['place_coordinates']->toWKT(),$parkingData['place_point']->toWKT(), $parking->id]);
                } catch (\Exception $exception) {
                    Log::error($exception);
                }
            }else{

                try {
                    $parking = Parking::create(array_only($parkingData, $this->databaseColumns));
                    DB::update('update parkings set place_coordinates = GeomFromText(?),place_point=GeomFromText(?) where id = ?',[$parkingData['place_coordinates']->toWKT(),$parkingData['place_point']->toWKT(), $parking->id]);
                } catch (\Exception $exception) {
                    Log::error($exception);
                }
            }

            if ($parking) {
                $parking->prices()->delete();
                foreach ($parkingData['Cells']['Tariffs'] as $tariff) {
                    $price = $parking->prices()->create([
                        'hour_price' => $tariff['HourPrice'],
                        'time_range' => $tariff['TimeRange'],
                        'first_half_hour' => $tariff['FirstHalfHour'],
                        'first_hour' => $tariff['FirstHour'],
                        'following_hours' => $tariff['FollowingHours'],
                        'tariff_type' => $tariff['TariffType']
                    ]);

                    if (strpos($tariff['TimeRange'], '-') !== false) {
                        $timeData = explode('-', $tariff['TimeRange']);
                        $price->update([
                            'start_time' => $timeData[0].':00',
                            'end_time' => $timeData[1].':00'
                        ]);
                    }
                }
            }

        }
    }

    protected function makePoint($coordinates)
    {
        return new Point($coordinates[0][0][1], $coordinates[0][0][0]);
    }
    protected function makeCoordinates($coordinates)
    {
        $point_array = [];
        foreach ($coordinates[0] as $coordinate) {
            $point_array[] = new Point($coordinate[1], $coordinate[0]);
        }
        return new LineString($point_array);
    }


}