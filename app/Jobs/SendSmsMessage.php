<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendSmsMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $phone;

    private $text;

    private $url = 'http://176.53.160.154/goip/en/';

    private  function fullUrl($part_url)
    {
        return $this->url . $part_url;
    }

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($phone, $text)
    {
        $this->phone = $phone;
        $this->text = $text;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = new Client();
        $response = $client->get($this->fullUrl('dosend.php'), [
            'query'=>[
                'USERNAME'=>config('goip.username'),
                'PASSWORD'=>config('goip.password'),
                'smsprovider'=>2,
                'goipname'=>'ks2',
                'smsnum'=>$this->phone == "7757" ? "757" : substr($this->phone,2),
                'method'=>2,
                'Memo'=>$this->text
            ]
        ])->getBody()->getContents();
        $array = explode("'", $response);
        $client->get($this->fullUrl($array[3]))->getStatusCode();
    }
}
