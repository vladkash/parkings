<?php

namespace App\Jobs;

use App\Car;
use App\ClientParking;
use App\Http\Controllers\Sms\Sms;
use App\Parking;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class StopParking implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $car;
    private $parking;
    private $clientParking;

    /**
     * Create a new job instance.
     *
     * @param Parking $parking
     * @param Car $car
     * @param ClientParking $clientParking
     */
    public function __construct(Parking $parking, Car $car, ClientParking $clientParking)
    {
        $this->car = $car;
        $this->parking = $parking;
        $this->clientParking = $clientParking;
    }

    /**
     *
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!$this->checkMoney()) {
            Sms::sendStopParkingNotification($this->car->client);
            return;
        }
        Log::alert("Stop parking");
        Sms::sendStopParkingMessage($this->car);
        StartParking::dispatch($this->parking, $this->car, $this->clientParking)->delay(now()->addMinutes(5));
    }

    private function checkMoney()
    {
        return true;
    }
}
