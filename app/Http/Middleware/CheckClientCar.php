<?php

namespace App\Http\Middleware;

use App\Car;
use App\Http\Controllers\API\ApiResponse;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckClientCar
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->has('id')){
            $car = Car::find($request->id);
            if($car->client->id !== Auth::user()->id){
                return response()->json(new ApiResponse(false, null, "Доступ запрещен"));
            }
        }
        return $next($request);
    }
}
