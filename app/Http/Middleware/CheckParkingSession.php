<?php

namespace App\Http\Middleware;

use App\Http\Controllers\API\ApiResponse;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckParkingSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $client = Auth::user();

        $currentParking = $client->parkings()->where('done', false)->first();

        if (!$currentParking) {
            return response()->json(new ApiResponse(false, null, "Отсутствует активная парковочная сессия"));
        }

        return $next($request);
    }
}
