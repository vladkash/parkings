<?php

namespace App\Http\Controllers;

use App\Http\Controllers\API\ApiResponse;
use Illuminate\Http\Request;

class RedirectController extends Controller
{
    public function redirectToAdminPanel()
    {
        return redirect('/nova/login');
    }

    public function getLogin()
    {
        return response()->json(new ApiResponse(false, null, 'Неверный токен доступа'));
    }
}
