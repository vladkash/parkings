<?php

namespace App\Http\Controllers\Sms;

use App\Car;
use App\Client;
use App\ClientParking;
use App\Http\Controllers\Controller;
use App\Jobs\SendSmsMessage;
use App\Parking;


class Sms extends Controller
{
    private static $parkingNumber = "7757";

    public static function sendAuthMessage($phone, $code)
    {
        $text_message = "Код для входа СитиПаркинг: " . $code;

        SendSmsMessage::dispatch($phone, $text_message);
    }

    public static function sendStartParkingMessage(Parking $parking, Car $car)
    {
        $text_message = $parking->place_id."*".$car->number.$car->regionnumber."*1";

        $client_parking = $car->parkings()->where('done',false)->first();

        if($client_parking){
            $client_parking->sms()->create([
                'message' => $text_message
            ]);
        }

        SendSmsMessage::dispatch(static::$parkingNumber, $text_message);
    }

    public static function sendStopParkingMessage(Car $car)
    {
        $text_message = "С*".$car->number.$car->regionnumber;

        $car->parkings()->where('done',false)->first()->sms()->create([
            'message' => $text_message
        ]);

        SendSmsMessage::dispatch(static::$parkingNumber, $text_message);
    }

    public static function sendStopParkingNotification($client)
    {
        $text_message = "На вашем балансе закончились деньги! Парковка приостановлена";

        SendSmsMessage::dispatch($client->phone, $text_message);
    }
}
