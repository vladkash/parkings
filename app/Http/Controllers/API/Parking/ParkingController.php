<?php

namespace App\Http\Controllers\API\Parking;

use App\Car;
use App\ClientParking;
use App\Http\Controllers\API\Accountant;
use App\Http\Controllers\API\ApiResponse;
use App\Http\Requests\Parking\SearchParking;
use App\Http\Requests\Parking\StartParking;
use App\Parking;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class ParkingController extends Controller
{
    public function index()
    {
        $parkings = Parking::select('id', 'place_coordinates')->get();

        return response()->json(new ApiResponse(true, $parkings));
    }

    public function show($id)
    {
        $parking = Parking::with('prices')->find($id);

        return response()->json(new ApiResponse(true, $parking));
    }

    public function start(StartParking $request)
    {
        $client = Auth::user();

        $clientParking =$client->parkings()->create($request->all());

        \App\Jobs\StartParking::dispatch(Parking::find($request->parking_id), Car::find($request->car_id), $clientParking)->delay(now()->addMinutes(4));

        return response()->json(new ApiResponse(true, $clientParking));
    }

    public function stop()
    {
        $client = Auth::user();

        $currentParking = $client->parkings()->where('done', false)->first();

        Accountant::accountParking($currentParking);

        $currentParking->finish();

        return response()->json(new ApiResponse(true, null, "Парковочная сессия окончена"));
    }

    public function search(SearchParking $request)
    {
        $parkings = Parking::where('place_name', 'LIKE','%'.$request->search.'%')->orWhere('address','LIKE','%'.$request->search.'%')->get();

        return response()->json(new ApiResponse(true, $parkings));
    }

    public function sessions()
    {
        $client = Auth::user();

        $parkingsHistory = $client->parkings()->orderBy('id','desc')->get();

        foreach ($parkingsHistory as $history) {
            $parking = $history->parking;
            if ($parking) {
                $history->parking_name = $parking->place_name;
                $history->metro = $parking->metro;

            }else{
                $history->parking_name = null;
                $history->metro = null;
            }
            $car = $history->car;
            if ($car) {
                $history->brand = $car->brand;
                $history->number = $car->number;
            }else{
                $history->brand = null;
                $history->number = null;
            }

            unset($history->car_id);
            unset($history->car);
            unset($history->parking_id);
            unset($history->parking);
        }

        return response()->json(new ApiResponse(true, $parkingsHistory));
    }

    public function currentSession()
    {
        $client = Auth::user();

        $current_session = $client->parkings()->with(['car','parking'])->where('done',false)->first();

        return response()->json(new ApiResponse(true, $current_session));
    }
}
