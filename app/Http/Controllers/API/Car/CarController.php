<?php

namespace App\Http\Controllers\API\Car;

use App\Http\Controllers\API\ApiResponse;
use App\Http\Requests\Car\CreateCar;
use App\Http\Requests\Car\UpdateCar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client = Auth::user();

        return response()->json(new ApiResponse(true, $client->cars));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCar $request)
    {
        $client = Auth::user();

        $car = $client->cars()->create($request->all());

        return response()->json(new ApiResponse(true, $car));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Auth::user();

        $car = $client->cars()->find($id);

        return response()->json(new ApiResponse(true, $car));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCar $request, $id)
    {
        $client = Auth::user();

        $client->cars()->find($id)->update($request->all());

        return response()->json(new ApiResponse(true, null, "Информация обновлена"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Auth::user();

        $client->cars()->find($id)->delete();

        return response()->json(new ApiResponse(true, null, "Машина удалена"));
    }
}
