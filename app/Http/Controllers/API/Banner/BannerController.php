<?php

namespace App\Http\Controllers\API\Banner;

use App\Banner;
use App\Http\Controllers\API\ApiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BannerController extends Controller
{
    public function show()
    {
        $banner = Banner::orderBy('showed')->first();

        $banner->updateViews();

        return response()->json(new ApiResponse(true, $banner));
    }
}
