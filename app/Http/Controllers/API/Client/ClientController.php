<?php

namespace App\Http\Controllers\API\Client;

use App\Http\Controllers\API\ApiResponse;
use App\Http\Requests\Client\UpdateRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ClientController extends Controller
{
    public function update(UpdateRequest $request)
    {
        $client = Auth::user();

        $client->update($request->only('name','email','phone'));

        $client->fresh();

        return response()->json(new ApiResponse(true, $client));
    }
}
