<?php

namespace App\Http\Controllers\API\Auth;

use App\Client;
use App\Http\Controllers\API\ApiResponse;
use App\Http\Requests\Auth\LoginClient;
use App\Http\Requests\Auth\RegisterClient;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{

    public function register(RegisterClient $request)
    {
        $data = $request->only('phone');

        $client = Client::create($data);

        $client->token = $client->createToken('Mobile app access')->accessToken;

        return response()->json(new ApiResponse(true, $client));
    }

    public function login(LoginClient $request)
    {
        $client = Client::getByPhone($request->phone);

        $client->token = $client->createToken('Mobile app access')->accessToken;

        return response()->json(new ApiResponse(true, $client));
    }
}
