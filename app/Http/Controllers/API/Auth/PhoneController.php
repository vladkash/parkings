<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\API\ApiResponse;
use App\Http\Requests\Auth\CheckPhone;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PhoneController extends Controller
{
    public function check(CheckPhone $request)
    {
        $data = $request->validated();
        return response()->json(new ApiResponse(true, $data, "Выбранный телефон подходит"));
    }
}
