<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\API\ApiResponse;
use App\Http\Controllers\Sms\Sms;
use App\Http\Requests\Auth\SendSms;
use App\SmsCode;
use App\Http\Controllers\Controller;

class SmsController extends Controller
{
    public function sendAuthSms(SendSms $request)
    {
        $data = $request->validated();

        $code = SmsCode::createCode($data['phone']);

        Sms::sendAuthMessage($data['phone'], $code);

        return response()->json(new ApiResponse(true, null, "Сообщение отправлено"));
    }
}
