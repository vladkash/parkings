<?php

namespace App\Http\Controllers\API\Callback;

use App\CallbackMessage;
use App\Http\Controllers\API\ApiResponse;
use App\Http\Requests\Callback\CallbackMessage as RequestCallback;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CallbackController extends Controller
{
    public function callback(RequestCallback $request)
    {
        $client = Auth::user();

        $callback_message = $client->callback()->create($request->validated());

        return response()->json(new ApiResponse(true, $callback_message));
    }
}
