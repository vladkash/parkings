<?php

namespace App\Http\Controllers\API\Payment;

use App\Http\Controllers\API\ApiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    public function index()
    {
        $client = Auth::user();

        $payments = $client->payments()->orderBy('id','desc')->get();

        return response()->json(new ApiResponse(true, $payments));
    }
}
