<?php

namespace App\Http\Controllers\API;


use App\Car;
use App\ClientParking;
use App\Parking;
use Illuminate\Support\Facades\Auth;

class Checker
{
    public static function checkCar($car_id)
    {
        $client = Auth::user();

        $car = Car::find($car_id);

        return $car ? $car->client->id === $client->id : true;
    }

    public static function checkExistsParkings()
    {
        $client = Auth::user();

        return !ClientParking::checkExistsClientParkings($client->id);
    }
}
