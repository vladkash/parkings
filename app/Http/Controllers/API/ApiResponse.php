<?php

namespace App\Http\Controllers\API;


class ApiResponse
{
    public $success;
    public $data;
    public $message;
    public $errors;

    public function __construct($success, $data, $message = null, $errors = null)
    {
        $this->success = $success;
        $this->data = $data;
        $this->message = $message;
        $this->errors = $errors;
    }

}