<?php

namespace App\Http\Requests\Parking;

use App\Http\Requests\BaseApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class SearchParking extends BaseApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'search'=>'required|string'
        ];
    }

    public function messages()
    {
        return [
            'search.required'=>'Введен пустой поисковый запрос',
            'search.string'=>'Поисковый запрос должен быть строкой'
        ];
    }
}
