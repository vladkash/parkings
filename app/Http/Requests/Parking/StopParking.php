<?php

namespace App\Http\Requests\Parking;

use App\ClientParking;
use App\Http\Requests\BaseApiRequest;
use Illuminate\Support\Facades\Auth;

class StopParking extends BaseApiRequest
{
    public function authorize()
    {
        $client = Auth::user();

        return ClientParking::checkExistsClientParkings($client->id);
    }

}
