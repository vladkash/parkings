<?php

namespace App\Http\Requests\Parking;

use App\Car;
use App\Http\Controllers\API\Checker;
use App\Http\Requests\BaseApiRequest;


class StartParking extends BaseApiRequest
{
    public function authorize()
    {
        return Checker::checkCar($this->car_id) && Checker::checkExistsParkings();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'parking_id'=>'required|exists:parkings,id',
            'car_id' => 'required|exists:cars,id',
        ];
    }

    public function messages()
    {
        return [
            'parking_id.required'=>'Укажите номер парковки',
            'parking_id.exists'=>'Парковка не найдена',
            'car_id.required'=>'Укажите ваш автомобиль',
            'car_id.exists'=>'Автомобиль не найден'
        ];
    }
}
