<?php

namespace App\Http\Requests;

use App\Http\Controllers\API\ApiResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class BaseApiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(new ApiResponse(false, null, "Ошибки валидации", $validator->errors())));
    }

    protected function failedAuthorization()
    {
        throw new HttpResponseException(response()->json(new ApiResponse(false, null, "Доступ запрещен")));
    }
}
