<?php

namespace App\Http\Requests\Client;

use App\Http\Requests\BaseApiRequest;
use App\Rules\CheckValidPhone;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateRequest extends BaseApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|min:3|max:20',
            'email' => [
                'email',
                Rule::unique('clients')->ignore(Auth::user()->id),
            ],
            'phone' => [
                Rule::unique('clients')->ignore(Auth::user()->id),
                new CheckValidPhone()
            ]
        ];
    }
}
