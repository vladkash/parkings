<?php

namespace App\Http\Requests\Callback;

use App\Http\Requests\BaseApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class CallbackMessage extends BaseApiRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email'=>'required|email',
            'message'=>'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Введите имя',
            'name.string' => 'Имя должно быть строкой',
            'email.required' => 'Введите email',
            'email.email' => 'Неккоректный email адрес',
            'message.required' => 'Введите сообщение'
        ];
    }
}
