<?php

namespace App\Http\Requests\Car;

use App\Http\Requests\BaseApiRequest;

class CreateCar extends BaseApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand' => 'required|string',
            'number' => 'required|string',
            'regionnumber'=>'required_if:isspecial,0|numeric|nullable',
            'isspecial' => 'required|boolean',
            'ismain' => 'required|boolean'
        ];
    }

    public function messages()
    {
        return [
            'brand.required' => 'Необходимо указать имя машины',
            'number.required' => 'Необходимо указать номер машины',
            'regionnumber.required' => 'Необходимо указать код региона',
            'regionnumber.numeric' => 'Код региона должен быть числом',
            'isspecial.required' => 'Необходимо ввести тип номера',
            'isspecial.boolean' => 'Тип номера должен быть булевым параметром',
            'ismain.required' => 'Необходимо указать информацию о том, машина ли это по умолчанию',
            'ismain.boolean' => 'Машина по умолчанию - булевый параметр'
        ];
    }
}
