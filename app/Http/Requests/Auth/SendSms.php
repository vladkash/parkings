<?php

namespace App\Http\Requests\Auth;

use App\Http\Controllers\API\ApiResponse;
use App\Http\Requests\BaseApiRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class SendSms extends BaseApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone'=>'required'
        ];
    }

    public function messages()
    {
        return [
            'phone.required' => 'Необходимо ввести номер телефона'
        ];
    }


}
