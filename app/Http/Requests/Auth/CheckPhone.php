<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\BaseApiRequest;
use Illuminate\Foundation\Http\FormRequest;
use App\Rules\CheckValidPhone;

class CheckPhone extends BaseApiRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone'=>['unique:clients']
        ];
    }

    public function messages()
    {
        return [
            'phone.unique'=>'Пользователь с таким телефоном уже зарегистрирован',
        ];
    }
}
