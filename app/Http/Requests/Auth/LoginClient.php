<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\BaseApiRequest;
use App\Rules\CheckSmsCode;
use Illuminate\Foundation\Http\FormRequest;

class LoginClient extends BaseApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'bail|required|exists:sms_codes|exists:clients',
            'code' => ['required', new CheckSmsCode($this->phone)]
        ];
    }

    public function messages()
    {
        return [
            'code.required' => 'Введен пустой код',
            'phone.required' => 'Необходимо ввести номер телефона',
            'phone.exists' => 'Необходимо отправить смс на телефон или зарегистрироваться'
        ];
    }
}
