<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\BaseApiRequest;
use App\Rules\CheckSmsCode;
use App\Rules\CheckValidPhone;
use Illuminate\Foundation\Http\FormRequest;

class RegisterClient extends BaseApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => ['bail','required','exists:sms_codes','unique:clients',new CheckValidPhone()],
            'code' => ['required', new CheckSmsCode($this->phone)]
        ];
    }

    public function messages()
    {
        return [
            'phone.unique'=>'Пользователь с таким телефоном уже зарегистрирован',
            'phone.required' => 'Необходимо ввести номер телефона',
            'phone.exists' => 'Необходимо отправить смс на телефон',
            'code.required' => 'Необходимо ввести код'
        ];
    }
}
