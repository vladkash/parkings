<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $guarded = [];

    public function parking()
    {
        return $this->belongsTo(Parking::class);
    }
}
