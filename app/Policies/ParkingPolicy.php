<?php

namespace App\Policies;

use App\User;
use App\Parking;
use Illuminate\Auth\Access\HandlesAuthorization;

class ParkingPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the parking.
     *
     * @param  \App\User  $user
     * @param  \App\Parking  $parking
     * @return mixed
     */
    public function view(User $user, Parking $parking)
    {
        return true;
    }

    /**
     * Determine whether the user can create parkings.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the parking.
     *
     * @param  \App\User  $user
     * @param  \App\Parking  $parking
     * @return mixed
     */
    public function update(User $user, Parking $parking)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the parking.
     *
     * @param  \App\User  $user
     * @param  \App\Parking  $parking
     * @return mixed
     */
    public function delete(User $user, Parking $parking)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the parking.
     *
     * @param  \App\User  $user
     * @param  \App\Parking  $parking
     * @return mixed
     */
    public function restore(User $user, Parking $parking)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the parking.
     *
     * @param  \App\User  $user
     * @param  \App\Parking  $parking
     * @return mixed
     */
    public function forceDelete(User $user, Parking $parking)
    {
        return false;
    }
}
