<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CheckValidPhone implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $trimmed_value = trim($value);
        return $this->checkFirstSymbols($trimmed_value) && $this->checkFirstSymbols($trimmed_value);
    }

    public function checkLength($phone)
    {
        return strlen($phone) == 12;
    }

    public function checkFirstSymbols($phone)
    {
        return substr($phone, 0, 2) == '+7';
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Неккоректный формат телефона.';
    }
}
