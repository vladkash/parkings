<?php

namespace App\Rules;

use App\SmsCode;
use Illuminate\Contracts\Validation\Rule;

class CheckSmsCode implements Rule
{
    protected $phone;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($phone)
    {

        $this->phone = $phone;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return SmsCode::checkCode($this->phone, $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Неверный код из смс!';
    }
}
