<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $guarded = [];

    protected $hidden = ['showed'];

    protected $dates = [
        'created_at',
        'finish_at'
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'finish_at'=>'datetime'
    ];

    public function updateViews()
    {
        $this->showed += 1;
        $this->save();
    }
}
