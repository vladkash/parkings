<?php

namespace App\Console\Commands;

use App\Jobs\ProcessParkingsData;
use Illuminate\Console\Command;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;


class ParseParkings extends Command
{
    private $apiKey = "3e50cdb27948c6eeec9c82b39c3b23e6";

    private $versionNumber;

    private $releaseNumber;

    protected $parseUrl = 'https://apidata.mos.ru/v1/datasets/623/rows';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parkings:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update parkings info in database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Get parkings data...');
        $data = $this->getParkingsData();
        ProcessParkingsData::dispatch($data);
        $this->info('Processing finished!');
        return true;
    }


    private function getParkingsData()
    {
        $client = new Client();
        return $client->get($this->parseUrl,['query' => ['api_key' => '3e50cdb27948c6eeec9c82b39c3b23e6']])->getBody()->getContents();
    }
}