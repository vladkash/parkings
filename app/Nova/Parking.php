<?php

namespace App\Nova;

use Davidpiesse\Map\Map;
use Davidpiesse\NovaMap\NovaMap;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;

class Parking extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Parking';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'place_name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'place_id',
        'place_name',
        'address',
        'hours',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Text::make("Номер парковки",'place_id')->sortable()->onlyOnDetail(),
            Text::make("Название",'place_name')->sortable(),
            Text::make("Адрес",'address')->sortable(),
            Text::make("Часы работы",'hours')->sortable()->onlyOnDetail(),

            Map::make("Расположение", 'place_point')->spatialType('Point') ->height('300px'),

            HasMany::make("Тарифы",'prices', 'App\Nova\Price'),
            HasMany::make("История парковки",'history', 'App\Nova\ClientParking'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    public static function label()
    {
        return 'Парковки';
    }

    public static function singularLabel()
    {
        return 'Парковку';
    }
}
