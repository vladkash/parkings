<?php

namespace App;

use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Laravel\Scout\Searchable;


/**
 * @property \Grimzy\LaravelMysqlSpatial\Types\LineString $place_coordinates
 */
class Parking extends Model
{
    use SpatialTrait;

    protected $spatialFields = [
        'place_coordinates',
        'place_point'
    ];

    protected $guarded = [];

    public function history()
    {
        return $this->hasMany(ClientParking::class);
    }

    public function prices()
    {
        return $this->hasMany(Price::class);
    }

    public static function checkForExists($place_name)
    {
        return static::where('place_name', $place_name)->first();
    }

    public function setPlaceCoordinatesAttribute($value)
    {
        $this->attributes['place_coordinates'] = json_encode($value);
    }

    public function setPlacePriceAttribute($value)
    {
        floatval($value) ? $this->attributes['place_price'] = floatval($value) : $this->attributes['place_price'] = 0;
    }
}
