<?php

namespace Davidpiesse\NovaMap;

use Laravel\Nova\Fields\Field;

class NovaMap extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'nova-map';
}
