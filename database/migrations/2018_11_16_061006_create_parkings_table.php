<?php

use Illuminate\Support\Facades\Schema;
use Grimzy\LaravelMysqlSpatial\Schema\Blueprint;


use Illuminate\Database\Migrations\Migration;

class CreateParkingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parkings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('place_id');
            $table->integer('place_external_id');
            $table->string('place_layer')->nullable();
            $table->string('place_hascounter')->nullable();
            $table->string('place_name');
            $table->double('place_price')->nullable();
            $table->lineString('place_coordinates')->nullable();
            $table->point('place_point')->nullable();
            $table->string('address');
            $table->string('hours')->nullable();
            $table->string('metro')->nullable();
            $table->string('linecolor')->nullable();
            $table->string('freespaces')->nullable();
            $table->string('percents_usage')->nullable();
            $table->string('color')->nullable();
            $table->string('layer_display_type')->nullable();
            $table->string('icon')->nullable();
            $table->text('additional')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parkings');
    }
}
