<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientParkingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_parkings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id');
            $table->integer('car_id');
            $table->integer('parking_id');
            $table->boolean('done')->default(false);
            $table->double('price')->default(0);
            $table->timestamp('finish_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_parkings');
    }
}
