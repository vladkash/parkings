<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemakeParkingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parkings', function (Blueprint $table) {
            $table->dropColumn('place_external_id');
            $table->dropColumn('place_layer');
            $table->dropColumn('place_hascounter');
            $table->dropColumn('metro');
            $table->dropColumn('linecolor');
            $table->dropColumn('freespaces');
            $table->dropColumn('percents_usage');
            $table->dropColumn('color');
            $table->dropColumn('layer_display_type');
            $table->dropColumn('icon');
            $table->dropColumn('additional');
            $table->string('hours')->default('Круглосуточно')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parkings', function (Blueprint $table) {
            //
        });
    }
}
