<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientParking extends Model
{
    protected $guarded = [];

    protected $casts = [
        'created_at'=>'datetime',
        'updated_at'=>'datetime',
        'finish_at'=>'datetime'
    ];
    protected $dates = [
        'created_at',
        'updated_at',
        'finish_at'
    ];
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function car()
    {
        return $this->belongsTo(Car::class );
    }

    public function parking()
    {
        return $this->belongsTo(Parking::class );
    }

    public static function checkExistsClientParkings($client_id)
    {
        return static::where([
            'client_id'=>$client_id,
            'done'=>false
        ])->first()?true:false;
    }

    public static function finish(ClientParking $parking)
    {
        $parking->update([
            'done'=>true,
            'finish_at'=>now()
        ]);
    }
}
