<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;


class Client extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $fillable = [
        'name', 'phone'
    ];

    public function cars()
    {
        return $this->hasMany(Car::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function parkings()
    {
        return $this->hasMany(ClientParking::class);
    }

    public static function getByPhone($phone)
    {
        return static::where('phone', $phone)->first();
    }
}
