<?php

namespace App\Console\Commands;

use App\Jobs\ProcessParkingsData;
use Illuminate\Console\Command;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;


class ParseParkings extends Command
{

    protected $parseUrl = 'http://parking.mos.ru/api/get/index.php';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parkings:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update parkings info in database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Get parkings data...');
        $data = $this->getParkingsData();
        /*$fp = fopen('data.txt', 'r');
        $data = fgets($fp);
        fclose($fp);*/
        ProcessParkingsData::dispatch($data)->delay(now()->addSeconds(1));
        $this->info('Processing started!');
        return true;
    }

    public function getParkingsData()
    {
        $client = new Client();
        return $client->get($this->parseUrl,['query' => ['zones' => '001,098,099,005,015','lang'=>'ru','price'=>'','pub'=>false,'project'=>false,'disabled'=>false]])->getBody()->getContents();
    }
}
