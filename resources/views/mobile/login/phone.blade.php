@extends("mobile.layout")

@section("content")
    <div class="login__mobile">
        <img src="{{asset('mobile/img/carimg.png')}}" alt="carimg">
    </div>
    <div class="main_login">
        <h2>Вход</h2>
        <p>Введите номер телефона для входа в <br> приложение</p>
        <div class="login_phone">
            <div class="login__phone">
                <input type="text" placeholder="+7 (965)">
                <p>Введите номер телефона</p>
            </div>
            <input type="submit" value="Войти">
        </div>
    </div>
@endsection