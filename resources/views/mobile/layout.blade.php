<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="{{asset('mobile/css/style.css')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>

@yield("content")

<script
        src="https://code.jquery.com/jquery-3.3.1.js"
        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>
<script src="{{asset('mobile/js/jquery.maskedinput.min.js')}}"></script>
<script src="{{asset('mobile/js/script.js')}}"></script>
</body>
</html>